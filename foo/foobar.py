from bar import Bar
import foo

class FooBar:
    def get_value(self):
        return Bar()

class BarFoo:
    def get_value(self):
        return foo.Foo()

    def other_value(self):
        return foo.Foo().get_value()
