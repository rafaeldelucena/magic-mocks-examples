import mock
import unittest

from foo.foobar import FooBar, BarFoo

class TestFooBar(unittest.TestCase):
    def setUp(self):
        pass

    def tearDown(self):
        pass

    @mock.patch("foo.foobar.Bar")
    def test_mocking_foobar_with_object_bar(self, b):
        foobar = FooBar()
        value = foobar.get_value()
        self.assertTrue(isinstance(value, mock.MagicMock), 'Is a MagicMock instance')

    @mock.patch("foo.foobar.foo.Foo")
    def test_mocking_barfoo_with_object_foo_foo(self, f):
        barfoo = BarFoo()
        value = barfoo.get_value()
        self.assertTrue(isinstance(value, mock.MagicMock), 'Is a MagicMock instance')

    @mock.patch("foo.foobar.foo.Foo.get_value", return_value=0.99)
    def test_mocking_barfoo_with_object_foo_foo_other_value(self, f):
        barfoo = BarFoo()
        value = barfoo.other_value()
        self.assertEquals(value, 0.99)
