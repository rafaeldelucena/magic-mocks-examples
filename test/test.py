import mock
import unittest

from foo.bar import Bar
from foo.foo import Foo

class TestFooAndBar(unittest.TestCase):
    def setUp(self):
        pass

    def tearDown(self):
        pass

    @mock.patch("foo.foo.random", return_value=0.123456)
    def test_mocking_foo(self, rand):
        foo = Foo()
        value = foo.get_value()
        self.assertEquals(0.123456, value)

    @mock.patch("foo.bar.random.random", return_value=0.123456)
    def test_mocking_bar(self, rand):
        bar = Bar()
        value = bar.get_value()
        self.assertEquals(0.123456, value)
